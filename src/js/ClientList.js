/**
 * Created by root on 22/12/15.
 */

exports.clientDetails = function(req,res) {

    var db = req.db;
    var collection = db.collection('ClientList');

    var company = req.query["company"];
    var email = req.query["email"];

    if (company) {

        collection.find({"company" : company}).toArray(function (error, doc) {

            if (error) {

                console.log("Error : ", error);
                res.status(500).json(error);


            } else if (doc.length == 0) {
                res.send(404, 'Data not found');

            } else {

                if (doc.length > 0) {
                    var responsArray = [];

                    for (var i = 0; i < doc.length; i++) {
                        var setData = {
                            "name": doc[i].name,
                            "email": doc[i].email,
                            "mobile": doc[i].mobile,
                            "address": doc[i].address,
                            "city": doc[i].city,
                            "company":doc[i].company
                        };

                        console.log("Response array is : " + setData);
                        responsArray.push(setData);
                    }
                    return res.json(responsArray, 200);
                }
            }

        });


    } else if(email) {

        collection.find({"email":email}).toArray(function (error, doc) {

            if (error) {

                console.log("Error : ", error);
                res.status(500).json(error);


            } else if (doc.length == 0) {
                res.send(404, 'Data not found');

            } else {

                if (doc.length > 0) {
                    var responsArray = [];

                    for (var i = 0; i < doc.length; i++) {
                        var setData = {
                            "name": doc[i].name,
                            "email": doc[i].email,
                            "mobile": doc[i].mobile,
                            "address": doc[i].address,
                            "city": doc[i].city,
                            "company":doc[i].company
                        };

                        console.log("Response array is : " + setData);
                        responsArray.push(setData);
                    }
                    return res.json(responsArray, 200);
                }
            }

        });

    }
};

exports.addClient = function(req, res) {

    var db = req.db;
    var collection = db.collection('ClientList');

    var name = req.body.name1;
    var email = req.body.email ;
    var mobile = req.body.mobile;
    var address = req.body.address ;
    var city = req.body.city ;
    var company = req.body.company ;


    var response = function (err, docs) {
        if (err) {
            return res.json(err, 500);
        }
        else {

            var successRes = {
                "status": "success",
                "name": docs[0].name,
                "email": docs[0].email,
                "address": docs[0].address,
                "mobile": docs[0].mobile,
                "city": docs[0].city,
                "company": docs[0].company
            };

//            var obj = JSON.parse( successRes );

            return res.jsonp(successRes , 200);
        }
    };

    function insertClient() {

        collection.insert({
                "name": name,
                "email": email,
                "address": address,
                "mobile": mobile,
                "city": city,
                "company":company
            }, {safe: true}, response
        );
    }

    if (!name || !email) {

        console.log("name and email is : " + name, email);
        res.send("Required parameters are missing.", 406);
        return;

    }else   {

        collection.find({"email" : email}).toArray(function(error, doc){

            if (error) {
                res.jsonp(error, 400);
            }
            else if (doc.length > 0) {
                var errRes = {
                    "status": "failed",
                    "message": "client is already exists"

                };
                res.send(errRes, 406); // not acceptable;
            }
            else {
                insertClient();
            }
        });
    }
};

exports.companyList = function(req, res) {

    var db = req.db;
    var collection = db.collection('CompanyList');

    var name = req.query["name"];

    if (name) {

        collection.find({"name":name}).toArray(function (error, doc) {

            if (error) {

                console.log("Error : ", error);
                res.status(500).json(error);


            } else if (doc.length == 0) {
                res.send(404, 'Data not found');

            } else {

                if (doc.length > 0) {
                    var responsArray = [];

                    for (var i = 0; i < doc.length; i++) {
                        var setData = {
                            "name": doc[i].name,
                            "image": doc[i].image,
                            "address": doc[i].address,
                            "contact": doc[i].contact,
                            "employees": doc[i].employees
                        };

                        console.log("Response array is : " + setData);
                        responsArray.push(setData);
                    }
                    return res.json(responsArray, 200);
                }
            }
        });
    }
    else {

        collection.find().toArray(function (error, doc) {

            if (error) {

                console.log("Error : ", error);
                res.status(500).json(error);


            } else if (doc.length == 0) {
                res.send(404, 'Data not found');

            } else {

                if (doc.length > 0) {
                    var responsArray = [];

                    console.log("Doc of 0th is : " + doc[0].address);
                    for (var i = 0; i < doc.length; i++) {
                        var setData = {
                            "name": doc[i].name,
                            "image": doc[i].image,
                            "address": doc[i].address,
                            "contact": doc[i].contact,
                            "employees": doc[i].employees
                        };

                        console.log("Response array is : " + setData);
                        responsArray.push(setData);
                    }
                    return res.json(responsArray, 200);
                }
            }
        });

    }
};


exports.addCompany = function(req, res) {

    var db = req.db;
    var collection = db.collection('CompanyList');

    var name = req.body.name1;
    var image = req.body.image ;
    var contact = req.body.contact;
    var address = req.body.address ;

    var response = function (err, docs) {
        if (err) {
            return res.json(err, 500);
        }
        else {

            var successRes = {
                "status": "success",
                "name": docs[0].name,
                "image": docs[0].image,
                "address": docs[0].address,
                "contact": docs[0].contact
            };

            return res.jsonp(successRes , 200);
        }
    };

    function insertClient() {

        collection.insert({
                "name": name,
                "image":image,
                "address": address,
                "contact": contact

            }, {safe: true}, response
        );
    }

    if (!name) {

        console.log("Response data is : "+ req.body.name1 );

        res.send("Required parameters are missing.", 406);
        return;

    }else   {

        collection.find({"name" : name}).toArray(function(error, doc){

            if (error) {
                res.jsonp(error, 400);
            }
            else if (doc.length > 0) {
                var errRes = {
                    "status": "failed",
                    "message": "Company is already exists"

                };
                res.send(errRes, 406); // not acceptable;
            }
            else {
                insertClient();
            }
        });
    }
};

exports.updateCompany = function(req,res){

    var db = req.db;
    var collection = db.collection('CompanyList');
    var name = (req.body.name=="" ||req.body.name==undefined)?"":req.body.name;
    var image = (req.body.image=="" ||req.body.image==undefined)?"":req.body.image;
    var contact = (req.body.contact=="" ||req.body.contact==undefined)?"":req.body.contact;
//    var address = (req.body.address=="" ||req.body.address==undefined)?"":req.body.address;
    var address = req.body.address;

    collection.find({"name":name}).toArray(function(error,docs){
        if (error || docs.length <= 0) {
            res.jsonp(406, "User not found");
            return;
        }
        else
        {
            collection.update({ "name": name}, {$set: { "name": name,
                    "image": image,
                    "contact": contact,
                    "address": address }},

                function (error, doc) {
                    if (error) res.json(error, 500);
                    else if (doc == 0) {
                        res.jsonp(404, 'data not found');
                        return;
                    }
                    else {

                        var successRes = {
                            "status": "success",
                            "name": name,
                            "image": image,
                            "contact": contact,
                            "address": address
                        };
                        return res.jsonp(successRes, 200);
                    }
                });
        }
    });
};


exports.updateEmployee = function(req,res){

    var db = req.db;
    var collection = db.collection('ClientList');
    var name = (req.body.name=="" ||req.body.name==undefined)?"":req.body.name;
    var email = (req.body.email=="" ||req.body.email==undefined)?"":req.body.email;
    var mobile = (req.body.mobile=="" ||req.body.mobile==undefined)?"":req.body.mobile;
    var address = (req.body.address=="" ||req.body.address==undefined)?"":req.body.address;
    var city = (req.body.city=="" ||req.body.city==undefined)?"":req.body.city ;
    var company = (req.body.company=="" ||req.body.company==undefined)?"":req.body.company ;

    collection.find({"email":email}).toArray(function(error,docs){
        if (error || docs.length <= 0) {
            res.jsonp(406, "User not found");
            return;
        }
        else
        {
            collection.update({ "email": email}, {$set: { "name": name,
                    "email": email,
                    "mobile": mobile,
                    "address": address,
                    "city":city,
                    "company":company
                }},

                function (error, doc) {
                    if (error) res.json(error, 500);
                    else if (doc == 0) {
                        res.jsonp(404, 'data not found');
                        return;
                    }
                    else {

                        var successRes = {
                            "status": "success",
                            "name": name,
                            "email": email,
                            "mobile": mobile,
                            "address": address,
                            "city":city,
                            "company":company
                        };
                        return res.jsonp(successRes, 200);
                    }
                });
        }
    });

};



