/**
 * Created by root on 22/12/15.
 * for polymer backend API
 */


var cors = require('cors');

var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var path = require('path');
var cookieParser = require('cookie-parser');
var engine = require('ejs-locals');

var app = express();
app.use(cors());
var fs = require('fs');

var MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
db;

var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var mongoClient = new MongoClient(new Server('localhost', 27017));
var ObjectID = require('mongodb').ObjectID;// to get id

mongoClient.open(function (err, mongoClient) {

        db = mongoClient.db("CompanyList");

    console.log("Connected correctly to server");

//       db.close();
});


app.use(function (req, res, next) {

    req.db = db;
    next();

});

app.use(morgan()); 					              // log every request to the console
app.use(methodOverride()); 					     // simulate DELETE and PUT
app.use(cookieParser());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// API call
var clientList = require('./js/ClientList');

console.log("Clientlist ....");

app.get('/clientDetails', clientList.clientDetails);
app.post('/addClient',clientList.addClient);
app.get('/companyList',clientList.companyList);
app.post('/addCompany',clientList.addCompany);
app.post('/updateCompany',clientList.updateCompany);
app.post('/updateEmployee',clientList.updateEmployee);


app.listen(27010, '0.0.0.0');